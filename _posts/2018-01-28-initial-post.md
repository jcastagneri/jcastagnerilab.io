---
layout: post
title:  "Initial Post"
date:   2018-01-28 18:49:32 -0700
categories: portfolio update
---

This is my first post to this Gitlab hosted website, and my first foray into web development in general!
I am excited to showcase things that I've worked on in further posts, and hopefully in a pleasing format. 

I'm using [Jekyll][jekyll-gh] as my static site generator. So far, it has been very intuitive to get the site
up and hosted in Gitlab. I posted my resume as an image asset in a separate page, and it displays in a very
natural format. Gitlab pages are an ideal format for this type of project being that I can push all the site
data to the Gitlab servers, manage versioning history, and chiefly, not have to worry about site management
when I'm not adding to the site.

Thanks for visiting!




[jekyll-gh]:   https://github.com/jekyll/jekyll                                                               
