---
layout: page
title: Academic Background
permalink: /academic/
---
{% include mathjax.html %}

# Planned Degrees
*Completed Fall 2018*
1. Minor in computer science
2. Bachelor's degree in engineering physics
3. Bachelor's degree in applied mathematics
4. Master's degree in applied mathematics

# My Time at CU Boulder
I entered CU Boulder in the Fall of 2014 as a physics student. At the time, I didn't particularly know why
I chose physics above anything else, but I consider myself lucky to find that I have made an excellent choice.

For this page, I thought I'd give a brief chronology of major academic events and projects during my time at CU.

### Freshman Year
Like many, my freshman year was a time of recentering in a wider world. I realized what a lot of things *were*
that year. As a short list:
* I learned differential equations and took a course in linear algebra. These both taught me what math actually
*is*--not computation, but abstract thinking! Upon this realization, I added the applied math major.
* Programming. I never touched a block of code before freshman year, and now I can't imagine a future for myself
without it. This is when I added the CS minor.

Foundational to my ensuing college carreer was my admittance to the Engineering Honors Program--a residential
college at CU focused not just on academic excellence, but self discovery and pursuit of meaning. These
philosophical questions will continue to dog me.

On the academic side, EHP is known for many things, one of which is continual victory in an international
competition in mathematical modeling (MCM). I competed in this competition with two friends and received an
honorable mention on our submission, available [here][plane]. You should know that part of the competition is
to write the entire paper in four days.

The MCM taught me a valuable lesson about the mechanics of applied math, although I didn't know it yet--it turns
out that everything is really just Newton's method in some form! The more math I learn, the more I come to
understand that the world of engineering is just the first order approximation.

### Sophomore Year
I ramped up my involvement in my second year.
#### Resident Adviser
I was an RA in the EHP dorm where I spent my first year. This means that I lived with the freshmen and both
guided and disciplined them through their first year. This is an incredibly difficult leadership position,
not only because it requires that you are available in a leadership capacity at any hour of the day, but also
because you must constantly hold yourself to a higher standard than you had before grown accustomed. The stress
of this role grew my capacity to handle a broad spectrum of duties more quickly than I had ever grown before.

#### The Coffee Bar
In the summer after my freshmen year, my friend Brendan Culliton and I agreed to build a coffee bar for the
EHP dorm. We were budgeted time and money--including a $1000 espresso machine--to construct a design of our choice. We decided to pursue
a design motif of 'rustic mineshaft', whatever that means, and got to designing. We completed the project in
January, but it didn't end there.

After construction of the coffee bar, I organized a 'bean team' to stock and clean the coffee bar equipment
on a daily schedule. Beyond this, I implemented different tactics to teach the community how to properly use and
clean coffee machines. Our enemy was 'tragedy of the commons', but luckily the bean team was succesful in
warding him off.

<img src="/assets/images/coffee_bar.jpg" width="360" height="270" />

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*The coffee bar*

This is the third year of a fully operational coffee bar manned by the bean team. I no longer
manage them--leadership of the group passes down through the classes. My hope is that this great boon to Andrews
Hall can persist for years to come. Yes, I still go and make free coffee all the time.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/assets/images/stella.jpg" width="360" height="270" />

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*The espresso machine*

#### The Philosophy Reading Group
Inspired by coursework we had seen as part of EHP, some friends and I formed a reading group for Martin
Heidegger's *Being and Time*. Up till that point the hardest tome I'd ever attempted, *Being and Time* is a
treatise seeking to explain how we exist within the world, and what it means to exist authentically. It took
us four months to finish the book, but it only made us hungry for more.

We continued the reading group, and still meet every Tuesday in Andrews Hall. New students join and older students
graduate, so we are always awash with new perspectives. We have read Camus, Sartre, Beauvoir, Nietzsche, Hume,
and more. Exposure to a broad spectrum of ideas has been monumental to my ability to think, act, and be present
in my life, and I owe that in large part to this group of students.

#### Senior Level Math Classes
My sophomore year also saw an increase in rigour in my academic life. I took senior level classes like applied
mathematical analysis, Fourier series with boundary value problems, and complex analysis. It was so exciting
to finally learn these mathematical tools which are so foundational to physics and engineering.

In my Fourier class, I did a project which derived the fast Fourier transform and applied it to music samples
to show themath behind the music recognition app, Shazam. The paper for this project can be found [here][FFT].

Complex analysis had been my hardest class at that point, and my project at the end of the course reflects that
difficulty--[this][zeta] paper formulates the Riemann zeta hypothesis, which is an unsolved problem detailing
the structure of prime numbers. If you are able to solve this problem, you will win eternal mathematical fame
and $1,000,000. I didn't solve it.  A corrolary of the Riemann zeta function allows me to say (with surprising
rigour) that $$\sum_{i=1}^{\infty}n=-\frac{1}{12}$$. This weirdness actually comes up in quantum field theory and
is what drew me to this topic in the first place.

### Junior Year
I started to think about my future after college more seriously in my junior year. I had a good idea by that
point which coursework interested me, and I had worked the previous summer at AMP Robotics. Both of these
factors led me to enroll in a concurrent BS/MS program in applied math--the master's degree would help my job
prospects within the field of machine learning, which is a pleasant excuse to be able to learn more cool math.

And that I did. I took a convex optimization course where I learned a framework for problem solving that is
employed on every logistics server in the world. For that course, I wrote a paper detailing a recent method
called 'convex relaxation' which allows convex optimization to be applied to non-convex problems. The paper
can be found [here][cvx].

I also took a course in asymptotics and perturbative methods. This class has helped my understanding of physics
and engineering so drastically that I can no longer imagine the world of modeling without it. I would describe
it in the following way: Most interesting quantitative problems in physics and engineering are far too complex
to be analytically or even numerically solvable. Asymptotics is a framework within which we can construct
analytical solutions to these problems that are 'good enough' for our use. For example, the problem of
turbulent fluid flow has no general solution, but by using asymptotic methods, you can derive the effects
of turbulence on some quantity you care about; in this case mean flow of the fluid. Details can be found [here][turb].

### Senior Year
This year, I have wrapped up nearly all of my graduate coursework. These courses include mathematical statistics,
partial differential equations, numerical analysis, statistical learning, solid state physics, and more. I'm
feeling more confident in my basis of knowledge and more effective in my ability to solve abstract problems.

With this comfort is growing restlessness--I'm itching to apply my knowledge! After graduating, I plan to
pursue a career in machine learning engineering. To prepare for this, I've been working in the field with
AMP Robotics for several years handling full stack deep learning infrastructure, but I'm also looking for
research opportunities within CU where I can apply what I know about machine learning (and ideally physics!)
towards a master's thesis topic.

### Final Semester
My final year saw coursework in stochastics, theoretical mechanics, cybersecurity, and jazz. For a course
in introductory cybersecurity, I wrote a simple version of SSL, which can be viewed [here](https://gitlab.com/jcastagneri/emulate_ssl_password_verification).
I also learned a great deal about financial derivatives pricing under stochastic modeling assumptions,
and I hope to pursue this field further in my own time. At the conclusion of this semester, I graduated
with my degrees in math and physics.


[plane]:{{ "/assets/pdfs/PLANE_AND_SIMPLE.pdf" | absolute_url }}
[zeta]:{{ "/assets/pdfs/Complex_Analysis_of_the_Riemann_Zeta_Function.pdf" | absolute_url }}
[FFT]:{{ "/assets/pdfs/FFT_Shazam_Project.pdf" | absolute_url }}
[cvx]:{{ "/assets/pdfs/multistage-convex-relaxation.pdf" | absolute_url }}
[turb]:{{ "/assets/pdfs/Turbulent_Diffusion.pdf" | absolute_url }}
