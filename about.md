---
layout: page
title: About
permalink: /about/
---
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/assets/images/profile_pic.jpg" width="250" height="250" />

#### Joe Castagneri
I'm a curious person who loves sharing ideas nearly as much as exploring them. A recent applied math
MSc grad at the University of Colorado at Boulder, I am embarking on a career in
machine learning engineering. I hope to use this website both to showcase my academic and work experience
related specifically to machine learning, but also to catalog other projects and hobbies that I am interested
in.

I built this website using Jekyll--you can find the source code on [GitHub][jekyll-organization].

[jekyll-organization]: https://github.com/jekyll
